# frozen_string_literal: true

class SaleService
  class Bankrupt < StandardError; end

  def initialize(inventory, market)
    @inventory = inventory
    @market = market
  end

  def buy_goods(new_lemons, new_sugars)
    return 'Not enough funds' if cash_is_negative?(new_lemons, new_sugars)

    add_lemons(new_lemons)
    add_sugars(new_sugars)
    take_money(new_lemons, new_sugars)
    @market.save && @inventory.save
    true
  end

  private

  def cash_is_negative?(new_lemons, new_sugars)
    @inventory.cash < (@inventory.cash - (new_lemons * @market.lemon_price) - (new_sugars * @market.sugar_price))
  end

  def add_lemons(new_lemons)
    @inventory.lemons = @inventory.lemons + new_lemons
  end

  def add_sugars(new_sugars)
    @inventory.sugar = @inventory.sugar + new_sugars
  end

  def take_money(new_lemons, new_sugars)
    @inventory.cash = @inventory.cash - (new_lemons * @market.lemon_price) - (new_sugars * @market.sugar_price)
  end
end