# frozen_string_literal: true

# Inventory interface
class InventoryInterface
  # Raised when there the debit request would bankrupt the business.
  class Bankrupt < StandardError; end

  # Raised when there aren't enough items in the inventory.
  class NotEnough < StandardError; end

  def initialize(inventory)
    @inventory = inventory
  end

  def copy
    Inventory.create(cash: @inventory.cash, lemons: @inventory.lemons,
                     sugar: @inventory.sugar, lemonades: @inventory.lemonades)
  end

  def credit(amount)
    added_value = @inventory.cash + amount
    @inventory.update(cash: added_value)
  end

  def take_lemons(amount)
    raise NotEnough unless enough_lemons?(amount)

    deducted_value = @inventory.lemons - amount
    @inventory.update(lemons: deducted_value)
  end

  def take_sugar(amount)
    raise NotEnough unless enough_sugar?(amount)

    deducted_value = @inventory.sugar - amount
    @inventory.update(sugar: deducted_value)
  end

  def make_lemonade(amount)
    raise NotEnough unless enough_sugar?(amount) && enough_lemons?(amount)

    take_lemons(amount)
    take_sugar(amount)

    added_value = @inventory.lemonades + amount
    @inventory.update(lemonades: added_value)
  end

  def take_lemonade(amount)
    raise NotEnough unless enough_lemonades?(amount)

    deducted_value = @inventory.lemonades - amount
    @inventory.update(lemonades: deducted_value)
  end

  private

  def valid_funds?(amount)
    @inventory.cash >= amount
  end

  def enough_lemons?(amount)
    @inventory.lemons >= amount && amount.positive?
  end

  def enough_sugar?(amount)
    @inventory.sugar >= amount && amount.positive?
  end

  def enough_lemonades?(amount)
    @inventory.lemonades >= amount && amount.positive?
  end
end
