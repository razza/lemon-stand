# frozen_string_literal: true

class ApplicationController < ActionController::Base
  protected

  def game_session
    GameSessionStorage.get_game(session)
  end
end
