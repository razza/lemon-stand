class MarketsController < ActionController::Base
  attr_reader :market, :inventory

  def new
    @market = Market.new(lemon_price: rand(0.25..0.5).round(2), sugar_price: rand(0.02..0.05).round(2))
    @inventory = Inventory.create(lemons: 10, sugar: 10, cash: 20) # current_inventory

  end

  def create
    @market = Market.new(lemon_price: given_params[:lemon_price].to_f, sugar_price: given_params[:sugar_price].to_f)
    sale_service = SaleService.new(current_inventory, @market)
    success = sale_service.buy_goods(given_params[:new_lemons].to_i, given_params[:new_sugars].to_i)
    if success == 'Not enough funds'
      render :new, status: :unprocessable_entity, flash: { notice: 'Whoops something went wrong' }
    else
      render :show, status: :created, flash: { notice: 'Successfully bought items', location: Market.last }
    end
  end

  def show
    @market = Market.find(params[:id])
  end

  private

  def current_inventory
    require 'inventory'
    @inventory = Inventory.last
  end

  def given_params
    params.require(:market).permit(%i[lemon_price sugar_price new_lemons new_sugars])
  end
end
