# Represents a single day in the game
class Day < ApplicationRecord
  belongs_to :game
  has_one :inventory
  has_one :market

  accepts_nested_attributes_for :inventory, :market

  # Will simulate a single day. Changes are committed to the database following the simulation.
  def simulate
    customer_count = people_passing_by
    raise GameOver, "You didn't have any customers." if customer_count.zero?

    inventory = InventoryInterface.new self.inventory
    lemonades = self.inventory.lemonades
    sold_lemonades = lemonades - people_passing_by

    # calculate the income from the lemonades
    income = sold_lemonades * lemonade_price

    # calculate the damage to the reputation
    if sold_lemonades.negative?
      reputation_hit = -sold_lemonades * 0.001
      self.reputation = reputation + reputation_hit
    end

    # remove the lemonades from the inventory
    self.inventory.cash = self.inventory.cash + income
    inventory.take_lemonade sold_lemonades
  end

  def people_passing_by
    temp_chance = 0.65 * ((temperature - 10) / 35) + 0.3
    temp_walkers = population * temp_chance

    walkers = temp_walkers * (1 - reputation.clamp(0, 1))
    walkers.floor.clamp(0, population)
  end

  class GameOver < StandardError; end

  private

  def end_of_day_reputation

  end

  def set_market_price

  end
end
