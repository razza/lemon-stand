# frozen_string_literal: true

# Represents the players inventory
# Stores the players lemons, cash, sugar and lemonades.
class Inventory < ApplicationRecord
  belongs_to :day
end
