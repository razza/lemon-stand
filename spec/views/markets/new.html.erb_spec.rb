require 'rails_helper'

RSpec.describe 'rendering the new template' do
  it 'displays the Marker with lemon and sugar price' do
    assign(:market, Market.new(lemon_price: 0.25, sugar_price: 0.02))

    render template: 'markets/new'

    expect(rendered).to match /0.25/
    expect(rendered).to match /0.02/
  end
end
