# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Inventory, type: :model do
  context 'created with no values' do
    let(:subject) { described_class.create }

    it 'sets cash to 5.0' do
      expect(subject.cash).to eq 5.0
    end

    it 'sets lemons to 0' do
      expect(subject.lemons).to eq 0
    end

    it 'sets sugar to 0' do
      expect(subject.sugar).to eq 0
    end

    it 'sets lemonades to 0' do
      expect(subject.lemonades).to eq 0
    end
  end

  context 'created with values' do
    let(:subject) { described_class.create(cash: 10, lemons: 10, sugar: 10, lemonades: 10) }

    it 'sets cash to 5.0' do
      expect(subject.cash).to eq 10
    end

    it 'sets lemons to 0' do
      expect(subject.lemons).to eq 10
    end

    it 'sets sugar to 0' do
      expect(subject.sugar).to eq 10
    end

    it 'sets lemonades to 0' do
      expect(subject.lemonades).to eq 10
    end
  end
end
