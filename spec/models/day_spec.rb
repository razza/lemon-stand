require 'rails_helper'

RSpec.describe Day, type: :model do
  describe '#simulate' do
    let(:game) { Game.create }
    let(:day) { DayFactory.new_day(game) }

    context 'with low temperature (10)' do
      before do
        day.population = 1000
        day.temperature = 10
      end

      it 'has a low number of customers passing by (300/1000)' do
        expect(day.send(:people_passing_by)).to eq 300
      end
    end

    context 'with high temperature (45)' do
      before do
        day.population = 1000
        day.temperature = 45
      end

      it 'has a high number of customers passing by (950/1000)' do
        expect(day.send(:people_passing_by)).to eq 950
      end
    end

    it 'lowers reputation if there are fewer lemonades than customers' do
      day.temperature = 45
      day.population = 1000  # there should be 950 customers coming past
      day.inventory.lemonades = 100

      day.simulate

      # there should be been 850 customers who didn't have their order fulfilled
      expect(day.reputation).to be 0.085
    end
  end
end
