# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_12_09_004324) do

  create_table "days", force: :cascade do |t|
    t.integer "game_id", null: false
    t.integer "day_count", null: false
    t.float "temperature", null: false
    t.float "lemonade_price"
    t.integer "population", null: false
    t.float "reputation", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["game_id"], name: "index_days_on_game_id"
  end

  create_table "games", force: :cascade do |t|
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "inventories", force: :cascade do |t|
    t.float "cash", default: 5.0
    t.integer "lemons", default: 0
    t.integer "sugar", default: 0
    t.integer "lemonades", default: 0
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "day_id", null: false
    t.index ["day_id"], name: "index_inventories_on_day_id"
  end

  create_table "markets", force: :cascade do |t|
    t.float "lemon_price"
    t.float "sugar_price"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  add_foreign_key "inventories", "days"
end
