# frozen_string_literal: true

# Migration creates the games table
class CreateGames < ActiveRecord::Migration[6.1]
  def change
    create_table :games, &:timestamps
  end
end
