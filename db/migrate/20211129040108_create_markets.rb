class CreateMarkets < ActiveRecord::Migration[6.1]
  def change
    create_table :markets do |t|
      t.float :lemon_price
      t.float :sugar_price

      t.timestamps
    end
  end
end
